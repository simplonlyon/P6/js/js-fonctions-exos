
let tab = ['ga', 'zo', 'bu', 'bu'];
//On appel la fonction search en lui donnant en argument le
//tableau qu'on a créé au dessus comme zone de recherche, et la
//valeur 'ga' comme valeur à chercher. On stock le retour de 
//cette fonction dans la variable result (optionnel)
let result = search(tab, 'ga');
//On affiche en console le nombre de fois qu'on a trouvé 'ga', donc 1
console.log(result);

// console.log(search(tab));
/**
 * La fonction search va permettre de rechercher
 * ce qu'on veut, là où on veut, et de compter
 * le nombre d'occurrence du truc en question.
 * Elle accepte les deux arguments suivant :
 * @param {any[]} toSearch le tableau dans lequel on cherche
 * @param {any} value la valeur qu'on cherche dans le tableau
 */
function search(toSearch, value) {
    //On commence par créer le compteur qui
    //dira combien de fois on a trouver la valeur
    //dans le tableau. Initialisé à zéro.
    let count = 0;
    //On fait une boucle sur le tableau (un for
    //classique ou un while marchent aussi)
    for (let item of toSearch) {
        //Si l'élément actuel du for correspond
        //à la valeur recherchée...
        if (item === value) {
            //...on incrémente le compteur
            count++;
        }
    }
    //On return le compteur pour laisser le choix
    //de quoi faire du résultat au moment de l'appel
    //de la fonction (plus maléable que de directement
    //exécuter l'action dans la fonction sans return)
    return count;
}


// for(let x = 0; x < toSearch.length; x++) {
//     let item = toSearch[x];
//     if (item === value) {
//         count++;
//     }

// }