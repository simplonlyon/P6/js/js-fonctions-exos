Mini TP fonction : (search-function.js)
fonction pour rechercher le nombre d'occurrence d'une valeur dans un tableau
1) Faire un nouveau fichier js et un nouveau fichier html (search-function.js/html par ex)
2) Essayer déjà sans fonction avec des valeurs en dure, genre ['ga','zo','bu', 'bu'], de compter le nombre d'occurence de 'bu' avec une boucle et de l'afficher en console(modifié)
(un for...of fait le taf, vous aurez besoin d'une variable compteur qui commence à zéro pour le nombre d'occurrence)
3) une fois que vous avez pu afficher 2 en console, transformer le code que vous avez en fonction
Pour ça, observer le code et se poser la question "qu'est-ce qui est succeptible de changer dans ce code ?", les éléments que vous identifiez seront les paramètres de la fonction
Donc remplacer ces éléments par des variables que vous mettrez dans les parenthèses de votre fonction
4) Faire que le console.log soit à l'extérieur de la fonction, donc faire que la fonction return le nombre d'occurrence trouvé