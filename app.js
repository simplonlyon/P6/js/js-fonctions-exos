"use strict"; // on active le mode strict de JavaScript https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode

// cette fonction retourne le prénom passé en paramètre concatainé avec la chaine de caractères "Bonjour ".
function bonjour(prenom) {
  return `Bonjour ${prenom}`;
}
// nous pouvons également utiliser une variable intermédiaire pour y stocker le résultat de la concaténation avant de le retourner.
function bonjour(prenom) {
  let result = `Bonjour ${prenom}`;
  return result;
}
// il est également possible de concatainer avec l'opérateur de chaine `+`
function bonjour(prenom) {
  let result = "Bonjour" + " " + prenom;
  return result;
}
// les trois versions de la fonction `bonjour` sont toutes équivalentes

// on peut ensuite afficher dans la console la valeur retournée par la fonction `bonjour`
console.log(bonjour("Monde")) // affiche "Bonjour Monde" dans la console (hello world #joke)

// cette fonction retourne la somme des entiers contenus dans un tableau
// par exemple somme([1, 2, 3]) retourne 6 (1+2+3)
function somme(arr) {// la fonction prend en argument un tableau appelé ici `arr`

  let result = 0; // on initialise la variable `result` qui va contenir le résultat de la somme des entiers du tableau `arr`

  for (let index = 0; index < arr.length; index++) {// pour chaque élément du tableau `arr` ...
    result += arr[index]; // ... on ajoute la valeur courante du tableau (arr[index]) à la variable `result`
  }

  return result // à la fin de la boucle la fonction retourne la valeur de result
}
let monTableau = [0, 1, 2, 3] // là encore, on peut directement mettre notre tableau en paramètres de la fonction ou on peut passer par une variable intermédiaire, ici `monTableau`
console.log(somme(monTableau)) // on peut ensuite afficher le résultat de la fonction dans la console
